package com.epam.autocode.calculator

class Calculator internal constructor(title: String) {
    init {
        println("Create a Calculator titled $title")
    }

    fun sum(first: Int, second: Int): Int {
        return first + second
    }

    fun subtract(first: Int, second: Int): Int {
        return first - second
    }

    fun multiply(first: Int, second: Int): Int {
        return first * second
    }

    fun divide(first: Int, second: Int): Int {
        return first / second
    }
}