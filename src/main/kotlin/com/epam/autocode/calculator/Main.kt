package com.epam.autocode.calculator

fun main(args: Array<String>) {

    val calculator = Calculator("demo")

    println(" (3  +4) = ${calculator.sum(3, 4)}")

    println(" (39 - 379) = ${calculator.subtract(39, 379)}")

}